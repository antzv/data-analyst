from datetime import datetime
import openpyxl
import os
# import big_query
import xlrd
import pandas as pd

df = pd.DataFrame()

# тестовый и ручной список отделений
# Использовался ранее и для тестовых целей

file_vers = datetime.today().strftime('%Y-%m-%d')

# dep = ['колопрокт']
# dep = ['ВИГ', 'гнойхир', 'пересадка почки', 'Пульмо', 'сосуд хир', 'сосуд хир 2', 'Торак 1', 'торак 2', 'урол', 'урол 2', 'Хирургия', 'колопрокт', 'нейрохир 2', 'нейрохир']

# Получаем список отделений

f_path = f'files/drug 310322_2'
dep = os.listdir(path=f'files/drug 310322_2')

# Обрабатываем файл для подготовки справочника по АТХ

dw = pd.read_excel('files/2021 Всё для авс по атх.xlsx', sheet_name='Лист1')
dw = dw[['Препарат', 'VEN', 'АТХ']].rename(columns={'Препарат': 'name', 'АТХ': 'ath'})
dw['ath'] = dw['ath'].str.replace('Противовоспалительные и противоревматические препараты', 'НПВП')
dw['name'] = dw['name'].str.capitalize()

# Формируем служебную таблицу для нумерации месяцев

d = {"num":   pd.Series([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
     "month": pd.Series(['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'])
     }

dfm = pd.DataFrame(d)

#

for j in dep:
    head_dir = os.listdir(path=f'{f_path}/{j}')
    for i in head_dir:
        wb_obj = xlrd.open_workbook_xls(f'{f_path}/{j}/{i}', formatting_info=True)
        ws = wb_obj.sheet_by_index(0)
        for x in range(1, ws.nrows):
            if ws.rowinfo_map[x].outline_level == 2 and len(ws._cell_values[x][0]) == 0:
                ws._cell_values[x][0] = str(ws._cell_values[x + 1][0]).split(' ', 1)[0]
            if ws.rowinfo_map[x].outline_level != 2:
                ws._cell_values[x][0] = 'deleted'
            s = pd.Series(ws.row_values(x)).append(pd.Series(i), ignore_index=True).append(pd.Series(j), ignore_index=True)
            df = df.append(s.T, ignore_index=True)

# Форматирование таблицы с данными

df = df[df[0] != 'deleted'].convert_dtypes().rename(columns={0: 'name', 7: 'consumption', 9: 'year', 10: 'department'})
df = df[['name',  'consumption', 'year', 'department']]
df['consumption'] = df['consumption'].replace('', '0')
df['name'] = df['name'].str.replace('*', '')
df['year'] = df['year'].str.replace('.xls', '').str.replace('Всё', '')
df['name'] = df['name'].str.capitalize()
df['month'] = df['year'].str.split(' ', 1).str[0]
df['month'] = df['month'].str.replace('июлб', 'июль').str.replace('Октябрь', 'октябрь')
df['year'] = df['year'].str.split(' ', 1).str[1]
df = df.dropna(subset=['name'])
ds = df.merge(dw,  how='left', left_on='name', right_on='name', indicator=True)
ds = ds.merge(dfm, how='left', left_on='month', right_on='month', indicator='exists')
ds = ds.drop_duplicates(subset=['name', 'consumption', 'year'])
ds = ds[['name', 'consumption', 'year', 'month', 'department', 'VEN', 'ath', 'num']]
ds = ds.astype({'consumption': 'float'})

# Блок для записи в Big Query===================

# ds = pd.read_excel('files/drags_all_month.xlsx')
# ds = ds.astype({'consumption': 'float'})
# ds = ds[['name', 'consumption', 'year', 'month', 'department', 'VEN', 'ath', 'num']]
# # print(ds)
#
# big_query.insert_big_query(ds, 'data-analyst-319914.medicine.drag_on_month', big_query.project_id)


# Блок проверок

# print(ds[ds['consumption'] == 1540])
# print(ds.columns)
# print(ds.dtypes)


# ================================= старый код оценить возможность удаления

# ds.to_excel(f'drags_{file_name}.xlsx')

# df  = pd.read_excel('drags_hir_month.xlsx')
# df1 = pd.read_excel('drags_pul_month.xlsx')
# df2 = pd.read_excel('drags_vig_month.xlsx')
#
# ds = pd.concat([df, df1, df2])
# ================================= старый код

# Запись данных в файл

# ds = ds[['name', 'consumption', 'year', 'month', 'department', 'VEN', 'АТХ']]
ds.to_excel(f'drags_all_month_{file_vers}.xlsx')
