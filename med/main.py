import pandas as pd
import os, re

path = os.listdir(path="files")
df_arr, x = [], 2012
# считывание файлов и их объединение в массив
for i in path:
    xl = pd.read_excel(f'files/{i}', sheet_name='RKB').drop(columns='Unnamed: 1')
    xl['year'] = f'{x}-12-31'
    df_arr.append(xl)
    x = x+1

# соединение набора датафреймов в один
y = pd.concat(df_arr)
# удаляем строку 'TOTAL' -- сумма по бактерии по всем отделениям
y = y[y['Название отделения'] != 'TOTAL']

for column in y:
    if column == 'Нет роста' or column == 'Название отделения' or column == 'ИТОГО' or column == 'year':
        continue
    else:
        # вычисляем процент по бактерии
        d = (y[column] / (y['ИТОГО'] - y['Нет роста'])) * 100
        # добавляем столбец с процентом во фрейм
        y[f'%_{column}'] = round(d, 2)

df = y.melt(id_vars=['Название отделения', 'year'], var_name='bacter').rename(columns={'Название отделения': 'Name', 'year': 'date'})

print(df)
# print(df.columns)
