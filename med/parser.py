import requests
from bs4 import BeautifulSoup
from time import sleep
from datetime import datetime
from googletrans import Translator
import pandas as pd

translator = Translator()
tr = translator.translate('hello world', dest='ru').text

site = 'https://pubmed.ncbi.nlm.nih.gov'
count_art = 20
term = 'covid-19'
url = f'https://pubmed.ncbi.nlm.nih.gov/?term={term}&filter=years.2016-2021&sort=pubdate&size={count_art}'
r = requests.get(url)
soup = BeautifulSoup(r.text, 'lxml')
# href = soup.find('div', class_='docsum-content').find('a', class_='docsum-title').get('href')
# # title_article = soup.find('div', class_='docsum-content').find('a', class_='docsum-title').text
# date_dd = BeautifulSoup(requests.get(f'https://pubmed.ncbi.nlm.nih.gov{href}').text, 'lxml').find('span', class_='secondary-date').text.strip()
# dd = datetime.strptime(date_dd[-11:], '%Y %b %d.').date()
# authors = soup.find('div', class_='docsum-content').find('span', class_='docsum-authors full-authors').text
# citation = soup.find('div', class_='docsum-content').find('span', class_='docsum-journal-citation full-journal-citation').text
# snippet = soup.find('div', class_='docsum-content').find('div', class_='full-view-snippet').text
# full_snippet = BeautifulSoup(requests.get(f'https://pubmed.ncbi.nlm.nih.gov{href}').text, 'lxml').find('div', class_='abstract-content selected').text

articls = soup.findAll('div', class_='docsum-content')
data = []

for art in articls[:3]:
    href = art.find('a', class_='docsum-title').get('href')
    title_article = translator.translate(art.find('a', class_='docsum-title').text.strip(), dest='ru').text
    authors = art.find('span', class_='docsum-authors full-authors').text.strip()
    # citation = art.find('span', class_='docsum-journal-citation full-journal-citation').text.strip()
    snippet = translator.translate(art.find('div', class_='full-view-snippet').text.strip(), dest='ru').text
    sleep(0.2)
    date_dd = BeautifulSoup(requests.get(f'https://pubmed.ncbi.nlm.nih.gov{href}').text, 'lxml').find('span', class_='secondary-date').text.strip()
    date = datetime.strptime(date_dd[-12:].strip(), '%Y %b %d.').date()
    data.append([href, title_article, authors, snippet, date])
    message = f'''
    {title_article}
    Описание: {snippet}
    Дата публикации: {date}
    Авторы: {authors}
    Ссылка на статью: {site + href}
    #{term}
    '''
    print(message)

# header = ['href', 'title_article', 'authors', 'snippet', 'date']
# df = pd.DataFrame(data, columns=header)
# print(df)

# df.to_csv('art.csv', sep=';')
