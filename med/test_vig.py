import datetime
import os
import openpyxl
import xlrd
import pandas as pd
import datetime as dt

ds = pd.DataFrame(columns=['name', 'count', 'sum', 'department', 'year'])
df = pd.DataFrame()

head_dir = os.listdir(path=f'files/drug 310322')

# Тестовый список отделений
# head_dir = ['Акуш отд обсерв']

# Обрабатываем файл для подготовки справочника по АТХ

dw = pd.read_excel('files/2021 Всё для авс по атх.xlsx', sheet_name='Лист1')
dw = dw[['Препарат', 'VEN', 'АТХ']].rename(columns={'Препарат': 'name', 'АТХ': 'ath'})
dw['ath'] = dw['ath'].str.replace('Противовоспалительные и противоревматические препараты', 'НПВП')
dw['name'] = dw['name'].str.capitalize()

# Формируем служебную таблицу для нумерации месяцев

d = {"num":   pd.Series([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
     "month": pd.Series(['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'])
     }

dfm = pd.DataFrame(d)

# сбор данных из папок по годам
# обработка файлов с расширением .xlsx
for i in head_dir:
    p = os.listdir(path=f'files/drug 310322/{i}')
    for j in p:
    # for j in ['август 2019.xlsx']:
        path = f'files/drug 310322/{i}/{j}'
        # загружаем эксель файл
        wb_obj = openpyxl.load_workbook(path)
        # активируем книгу эксель
        ws = wb_obj.active
        # получаем количество строк в книге
        m_row = ws.max_row
        for c in range(1, m_row + 1):
            if ws.row_dimensions[c].outlineLevel == 2 and len(ws.cell(row=c, column=1).value) == 0:
                cell_obj = ws.cell(row=c, column=1)
                cell_obj.value = str(ws.cell(row=c + 1, column=1)).split(' ', 1)[0]
            if ws.row_dimensions[c].outlineLevel != 2:
                cell_obj = ws.cell(row=c, column=1)
                cell_obj.value = 'deleted'
        for t in ws.iter_rows():
            lists = []
            for cell in t:
                lists.append(cell.value)
            s = pd.Series(lists).append(pd.Series(i), ignore_index=True).append(pd.Series(j), ignore_index=True)
            # print(s)
            df = df.append(s.T, ignore_index=True)


df = df[df[0] != 'deleted'].convert_dtypes().rename(columns={0: 'name', 7: 'consumption', 10: 'year', 9: 'department'})
df = df[['name',  'consumption', 'year', 'department']]
df['consumption'] = df['consumption'].replace('', '0')
df['name'] = df['name'].str.replace('*', '')
df['year'] = df['year'].str.replace('.xlsx', '').str.replace('Всё', '')
df['name'] = df['name'].str.capitalize()
df['month'] = df['year'].str.split(' ', 1).str[0]
df['month'] = df['month'].str.replace('июлб', 'июль').str.replace('Октябрь', 'октябрь')
df['year'] = df['year'].str.split(' ', 1).str[1]
df['year'] = df['year'].str.replace('2019..', '2019')
df = df.dropna(subset=['name'])
ds = df.merge(dw,  how='left', left_on='name', right_on='name', indicator=True)
ds = ds.merge(dfm, how='left', left_on='month', right_on='month', indicator='exists')
ds = ds.drop_duplicates(subset=['name', 'consumption', 'year'])
ds = ds[['name', 'consumption', 'year', 'month', 'department', 'VEN', 'ath', 'num']]
ds = ds.astype({'consumption': 'float'})

ds.to_excel('drags_testing.xlsx')
# print(ds)
