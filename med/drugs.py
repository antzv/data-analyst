import datetime
import os
import openpyxl
import xlrd
import pandas as pd
import datetime as dt

ds = pd.DataFrame(columns=['name', 'count', 'sum', 'department', 'year'])
df = pd.DataFrame()

head_dir = os.listdir(path='files/drugs2')
# сбор данных из папок по годам
# for i in head_dir:
#     p = os.listdir(path=f'drugs/{i}')
#     for j in p:
#         path = f'drugs/{i}/{j}'
#         wb_obj = openpyxl.load_workbook(path)
#         ws = wb_obj.active
#         m_row = ws.max_row
#         for c in range(1, m_row + 1):
#             if ws.row_dimensions[c].outlineLevel != 2:
#                 cell_obj = ws.cell(row=c, column=1)
#                 cell_obj.value = 'deleted'
#         df = pd.DataFrame(ws.values, columns=['A', 'B', 'C', 'D', 'I', 'F', 'G', 'H', 'T'])
#         df = df[df['A'] != 'deleted'].convert_dtypes().rename(columns={'A': 'name', 'G': 'count', 'H': 'summ'})
#         df = df[['name', 'count', 'summ']]
#         df['department'] = i
#         df['year'] = dt.datetime.strptime(str(j).replace(".xlsx", ""), '%Y').date()
#         ds = ds.append(df, ignore_index=True).dropna(subset=['name'])
#
# ds.to_excel('drags.xlsx')
# print(ds)

for i in head_dir:
    wb_obj = xlrd.open_workbook_xls(f'files/drugs2/{i}', formatting_info=True)
    ws = wb_obj.sheet_by_index(0)
    for x in range(1, ws.nrows):
        if ws.rowinfo_map[x].outline_level != 2:
            ws._cell_values[x][0] = 'deleted'
        s = pd.Series(ws.row_values(x)).append(pd.Series(i.replace('01.01-30.06.xls', '').replace('01.01.-30.06.xls', '')), ignore_index=True)
        df = df.append(s.T, ignore_index=True)

df['year'] = dt.datetime.strptime('30.06.2021', '%d.%m.%Y').date()
df = df[df[0] != 'deleted'].convert_dtypes().rename(columns={0: 'name', 6: 'number', 7: 'consumption', 9: 'department'})
df = df[['name', 'number', 'consumption', 'department', 'year']]
df = df.dropna(subset=['name'])
df['name'] = df['name'].str.replace('*', '')

df['% от общей суммы'] = df.consumption/df.groupby('department').consumption.transform('sum')
df.to_excel('drags2.xlsx')
