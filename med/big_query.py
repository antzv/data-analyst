from google.oauth2 import service_account
import pandas_gbq

project_id = 'data-analyst-319914'
credentials = service_account.Credentials.from_service_account_file('data-analyst-319914-e0151312e8c2.json')

pandas_gbq.context.credentials = credentials
pandas_gbq.context.project = project_id

# var SQL Query
table_from = 'cogent-anvil-319706.medicine.name2014'


def select_big_query(table):
    return pandas_gbq.read_gbq(f'SELECT * FROM `{table}`')


def insert_big_query(df, table, project_id):
    pandas_gbq.to_gbq(df, table, project_id, if_exists='replace')

# print(select_big_query(table_from))
