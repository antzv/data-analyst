import openpyxl
import os
import big_query
import xlrd
import pandas as pd

df = pd.DataFrame()
dep = 'по годам'
file_name = 'yyyy'
head_dir = os.listdir(path=f'files/{dep}')

# dw = pd.read_excel('files/2021 Всё для авс по атх.xlsx', sheet_name='Лист1')
# dw = dw[['Препарат', 'VEN', 'АТХ']].rename(columns={'Препарат': 'name', 'АТХ': 'ath'})
# dw['ath'] = dw['ath'].str.replace('Противовоспалительные и противоревматические препараты', 'НПВП').str.strip()


# dw['name'] = dw['name'].str.capitalize().str.strip()
# dw = pd.read_excel('drags_dict.xlsx')
# dw = dw[['name', 'VEN', 'ath']]
# dw['name'] = dw['name'].str.replace('+', ' + ').str.replace('  ', ' ').str.capitalize()
#
#
# for i in head_dir:
#     wb_obj = xlrd.open_workbook_xls(f'files/{dep}/{i}', formatting_info=True)
#     ws = wb_obj.sheet_by_index(0)
#     for x in range(1, ws.nrows):
#         ws._cell_values[x][1] = i # для расчетов по годам
#         # if ws.rowinfo_map[x].outline_level != 2:
#         #     ws._cell_values[x][0] = 'deleted'
#         if ws.rowinfo_map[x].outline_level == 2 and len(ws._cell_values[x][0]) == 0:
#            ws._cell_values[x][0] = str(ws._cell_values[x + 1][0]).split(' ', 1)[0]
#         if ws.rowinfo_map[x].outline_level != 2:
#             ws._cell_values[x][0] = 'deleted'
#         s = pd.Series(ws.row_values(x)).append(pd.Series(i), ignore_index=True)
#         df = df.append(s.T, ignore_index=True)
# #
# #
# df = df[df[0] != 'deleted'].convert_dtypes().rename(columns={0: 'name', 7: 'consumption', 1: 'year'})
# df = df[['name',  'consumption', 'year']]
# df['name'] = df['name'].str.replace('*', '').str.strip().str.capitalize().str.replace('+', ' + ').str.replace('  ', ' ')
# df['year'] = df['year'].str.replace('.xls', '').str.replace('Все', '')
# df = df.dropna(subset=['name'])
# df.astype
# ds = df.merge(dw, how='left', left_on='name', right_on='name', indicator=True)
# ds = ds.drop_duplicates(subset=['name', 'consumption', 'year'])
# ds = ds[['name', 'consumption', 'year', 'VEN', 'ath']]


# Загрузка из файла =========================
ds = pd.read_excel('files/drags_on_year_v_2.xlsx')
ds = ds.astype({'consumption': 'float'})
ds['ath'] = ds['ath'].str.strip()
ds = ds[['name', 'consumption', 'year', 'VEN', 'ath']]
# =================================================================

big_query.insert_big_query(ds, 'data-analyst-319914.medicine.drag_on_year', big_query.project_id)

print(ds.columns)
print(ds.dtypes)
# ds.to_excel('drags_on_year_v_2.xlsx')
# dw.to_excel('drags_dict.xlsx')
