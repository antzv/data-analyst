import cx_Oracle
import sqlalchemy

# настройка подключения
user = 'ADMIN'
pw = "Fhvzy,f,fzy1"
db = 'oraclecloud_medium'
port = '1522'
host = 'adb.uk-london-1.oraclecloud.com'

# подключение через cx_Oracle - выполнение SQL запросов через курсор
# connection = cx_Oracle.connect(user, pw, db)
# подключение через sqlalchemy - подключение и работа с БД через Pandas
engine = sqlalchemy.create_engine(f'oracle+cx_oracle://{user}:{pw}@{db}').connect()
