# импорт из файла handlers
from config import bot_token
# импорт из библиотеки aiogram
from aiogram import Bot, Dispatcher, executor
# импорт библиотеки для асинхронного программирования
import asyncio

loop = asyncio.get_event_loop()
bot = Bot(bot_token, parse_mode='HTML')
dp = Dispatcher(bot, loop=loop)

if __name__ == '__main__':
    from handlers import dp, send_to_admin
    executor.start_polling(dp, on_startup=send_to_admin)
