import requests
from bs4 import BeautifulSoup

term = 'covid-19'
url = f'https://pubmed.ncbi.nlm.nih.gov/?term={term}&sort=pubdate&sort_order=asc&size=20'

r = requests.get(url)
soup = BeautifulSoup(r.text, 'lxml')
print(r.text)
