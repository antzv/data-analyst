---
title: "Markdown"
output: 
  html_document:
    theme: sandstone
    highlight: tango
date: "`r Sys.Date()`"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Вывод таблицы в более эстетичном виде

```{r}
data <- faithful[1:4, ]
knitr::kable(data, 
 caption = "Table with kable")
```

# Вставка двух графиков рядом <br><br>

```{r echo=TRUE, fig.show='hold', out.width=c('50%', '50%')}
boxplot(1:10)
plot(rnorm(10))
```

# Растояние между строк {#split-link}

<br><br>

A first sentence <br><br><br><br> A seconde sentence<br><br>

# Создание нескольких колонок<br><br>

`col-md-4` означает, что данный блок будет занимать 4 условных единицы из 12 в строке, то есть треть ширины экрана устройства с экраном шириной от 992 пикселей

Все типы классов:

col-xs-\*: для устройств с шириной экрана меньше 768 пикселей

col-sm-\*: для устройств с шириной экрана от 768 пикселей и выше

col-md-\*: для устройств с шириной экрана от 992 пикселя и выше

col-lg-\*: для устройств с шириной экрана от 1200 пикселей и выше

`::: col-md-4`

`<br><br>Since R Markdown use the [bootstrap framework](https://getbootstrap.com/docs/4.0/layout/grid/) under the hood. It is possible to benefit its powerful grid system. Basically, you can consider that your row is divided in 12 subunits of same width. You can then choose to use only a few of this subunits.`

`:::`

`::: col-md-4`

`<br><br>Here, I use 3 subunits of size 4 (4x3=12). The last column is used for a plot. You can read more about the grid system [here](bootstrap%20grid%20system). I got this result showing the following code in my R Markdown document.`

`:::`

`::: col-md-4`

```` ```{r, message=FALSE, echo=FALSE} ````

`plot(rnorm(10))`

```` ``` ````

`:::`

::: col-md-4
<br><br>Since R Markdown use the [bootstrap framework](https://getbootstrap.com/docs/4.0/layout/grid/) under the hood. It is possible to benefit its powerful grid system. Basically, you can consider that your row is divided in 12 subunits of same width. You can then choose to use only a few of this subunits.
:::

::: col-md-4
<br><br>Here, I use 3 subunits of size 4 (4x3=12). The last column is used for a plot. You can read more about the grid system [here](bootstrap%20grid%20system). I got this result showing the following code in my R Markdown document.
:::

::: col-md-4
```{r, message=FALSE, echo=FALSE}
plot(rnorm(10))
```
:::

# Использование кнопок для под заголовков {.tabset .tabset-fade .tabset-pills}

------------------------------------------------------------------------

В заголовке верхнего уровня указываем код `{.tabset .tabset-fade .tabset-pills}` он сформируем из заголовков нижнего уровня кнопки. Далее после этой секции используем заголовок такого же уроня, как и тот к которому применен код

## First {.tabset .tabset-fade .tabset-pills}

A first section

### Один-один

надпись 1.1.

### Один-два

надпись 1.2.

## Second

content of sub-chapter #2

## Third

content of sub-chapter #3

# Вывод интерактивной таблицы <br><br>

```{r}
library(DT)
datatable(mtcars, rownames = FALSE, filter="top", options = list(pageLength = 5, scrollX=T) )
```

# Выделить фрагмент текста <br><br>

```{=html}
<style>
div.blue { background-color:#e6f0ff; border-radius: 5px; padding: 20px;}
</style>
```
::: blue
-   This is my first conclusion
-   This is my second conclusion
:::

# Внутренние ссылки<br><br>

Можно прикрепить к разделу внутренню ссылку таким образом:<br> `{#name_link}`

И потом вставить [ссылку](#split-link) по тексту, вот так:<br> `И потом вставить [ссылку](#split-link) по тексту`

# Интерактивные графики<br><br>

```{r}
library(ggplot2)
library(plotly)
library(gapminder)
 
p <- gapminder %>%
  filter(year==1977) %>%
  ggplot( aes(gdpPercap, lifeExp, size = pop, color=continent)) +
  geom_point() +
  scale_x_log10() +
  theme_bw()
 
ggplotly(p)
```

# Темы документа <br><br>

Для документа можно применять различные темы, указать тему нужно в YAML заголовке

`title: "your title" output:   html_document:     theme: sandstone     highlight: tango`

Сами темы можно найти на <https://bootswatch.com/>

<br><br>

# Больше про интерактивные таблицы {.tabset}

## Простой вывод таблицы

```{r}
library(DT)
datatable(iris)
```

Разберем атрибуты настройки вывода таблицы:

-   `datatable(head(mtcars), rownames = FALSE)` \# no row names

-   `datatable(head(mtcars), rownames = head(LETTERS))` \# new row names

-   \# colnames(iris) is a character vector of length 5, and we replace it

    `datatable(head(iris), colnames = c('Here', 'Are', 'Some', 'New', 'Names'))`

-   `datatable(head(iris), colnames = c('A Better Name' = 'Sepal.Width'))` - изменить имя столбца

-   `datatable(head(iris), colnames = c('Another Better Name' = 2, 'Yet Another Name' = 4))` - указать имя для столбца по номеру

-   `datatable(head(iris), caption = 'Table 1: This is a simple caption for the table.')` - добавить дополнительное описание к таблице

## Своя форма таблицы

```{r}
# a custom table container
sketch = htmltools::withTags(table(
  class = 'display',
  thead(
    tr(
      th(rowspan = 2, 'Species'),
      th(colspan = 2, 'Sepal'),
      th(colspan = 2, 'Petal')
    ),
    tr(
      lapply(rep(c('Length', 'Width'), 2), th)
    )
  )
))

# use rownames = FALSE here because we did not generate a cell for row names in
# the header, and the header only contains five columns
datatable(iris[1:20, c(5, 1:4)], container = sketch, rownames = FALSE)
```

## Фильтры таблиц

```{r}
iris2 = iris[c(1:10, 51:60, 101:110), ]
datatable(iris2, filter = 'top', options = list(
  pageLength = 5, autoWidth = TRUE
))
```

## Настройка вывода {.tabset}

Список дополнений можно посмотреть [здесь](https://datatables.net/extensions/index)

### Опции

```{r}
datatable(head(iris, 20), options = list(
  columnDefs = list(list(className = 'dt-center', targets = 5)), # 5 столбцов размещаем по центру
  pageLength = 5, # по умолчанию выводим по 5 строк
  lengthMenu = c(5, 10, 15, 20), # по сколько строк можно выводить
  order = list(list(2, 'asc'), list(4, 'desc')) # задаем сортировку столбцов
))
```

### Кнопки

```{r}
datatable(
  iris, extensions = 'Buttons', options = list(
    dom = 'Bfrtip',
    buttons = c('copy', 'csv', 'excel', 'pdf', 'print')
  )
)
```


### Менять местами столбцы

```{r}
datatable(iris2, extensions = 'ColReorder', options = list(colReorder = TRUE))
```

### Видимость столбцов

```{r}
# using I() here to make sure it is converted to an array in JavaScript
datatable(
  iris2, rownames = FALSE,
  extensions = 'Buttons', options = list(dom = 'Bfrtip', buttons = I('colvis'))
)
```

```{r}
# exclude the first two columns (i.e. they are always visible)
datatable(
  iris2, rownames = FALSE,
  extensions = 'Buttons', options = list(
    dom = 'Bfrtip',
    buttons = list(list(extend = 'colvis', columns = c(2, 3, 4)))
  )
)
```

### Фиксация столбца и заголовка

```{r}
m = as.data.frame(round(matrix(rnorm(100), 5), 5))
# fix some left 2 columns and right 1 column
datatable(
  m, extensions = 'FixedColumns',
  options = list(
    dom = 't',
    scrollX = TRUE,
    fixedColumns = list(leftColumns = 2, rightColumns = 1)
  )
)
```

```{r}
datatable(
  iris, extensions = 'FixedHeader',
  options = list(pageLength = 50, fixedHeader = TRUE)
)
```

### Активация ячейки

```{r}
datatable(iris2, extensions = 'KeyTable', options = list(keys = TRUE))
```

### Группировка строк

```{r}
mtcars2 = mtcars[1:20, ]
datatable(
  mtcars2[order(mtcars2$cyl), ],
  extensions = 'RowGroup',
  options = list(rowGroup = list(dataSrc = 2)),
  selection = 'none'
)
```

### Перетащить строку

Позволяет перемещать строки внутри таблицы

```{r}
datatable(
  iris2, colnames = c(ID = 1), extensions = 'RowReorder',
  options = list(rowReorder = TRUE, order = list(c(0 , 'asc')))
)
```

### Скрол строк

```{r}
m = matrix(runif(1000 * 4), ncol = 4, dimnames = list(NULL, letters[1:4]))
m = cbind(id = seq_len(nrow(m)), round(m, 2))
datatable(m, extensions = 'Scroller', options = list(
  deferRender = TRUE,
  scrollY = 200, # высота окна с данными
  scroller = TRUE
))
```



## Формат данных {.tabset}

### Начальный вариант

```{r}
library(DT)
m = cbind(matrix(rnorm(60, 1e5, 1e6), 20), runif(20), rnorm(20, 100))
m[, 1:3] = round(m[, 1:3])
m[, 4:5] = round(m[, 4:5], 7)
colnames(m) = head(LETTERS, ncol(m))
head(m)
```

### Вариант 2

```{r}
datatable(m) %>% formatCurrency(c('A', 'C')) %>% formatPercentage('D', 2)
```

### Вариант 3

```{r}
# the first two columns are Euro currency, and round column E to 3 decimal places
datatable(m) %>% formatCurrency(1:2, '\U20AC', digits = 0) %>% formatRound('E', 3)
```

## Стили {.tabset}

### Один столбец

```{r}
datatable(iris, options = list(pageLength = 5)) %>%
  formatStyle('Sepal.Length',  color = 'red', backgroundColor = 'orange', fontWeight = 'bold')
```

### Множество стоблцов

```{r}
datatable(iris) %>% 
  formatStyle('Sepal.Length', fontWeight = styleInterval(5, c('normal', 'bold'))) %>%
  formatStyle(
    'Sepal.Width',
    color = styleInterval(c(3.4, 3.8), c('white', 'blue', 'red')),
    backgroundColor = styleInterval(3.4, c('gray', 'yellow'))
  ) %>%
  formatStyle(
    'Petal.Length',
    background = styleColorBar(iris$Petal.Length, 'steelblue'),
    backgroundSize = '100% 90%',
    backgroundRepeat = 'no-repeat',
    backgroundPosition = 'center'
  ) %>%
  formatStyle(
    'Species',
    transform = 'rotateX(45deg) rotateY(20deg) rotateZ(30deg)',
    backgroundColor = styleEqual(
      unique(iris$Species), c('lightblue', 'lightgreen', 'lightpink')
    )
  )
```
