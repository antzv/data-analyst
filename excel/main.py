import openpyxl

path = '01.01-30.06.2021.xlsx'
wb_obj = openpyxl.load_workbook(path)
ws = wb_obj.active
m_row = ws.max_row

for i in range(1, m_row + 1):
    if ws.row_dimensions[i].outlineLevel == 3:
        cell_obj = ws.cell(row=i, column=1)
        print(f'yes --> {cell_obj.value}')
        cell_obj.value = 'deleted'

wb_obj.save(path)
