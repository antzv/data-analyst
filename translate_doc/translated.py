from google_trans_new import google_translator
from translate import Translator
import re


def translate(aa):
    # создаем объект переводчика
    gog_translator = google_translator()
    try:
        tr = gog_translator.translate(aa, lang_tgt='ru', lang_src='en').lower()
    except Exception:
        tr = Translator(to_lang='ru', from_lang='en').translate(aa)
        if re.fullmatch(r'(.*)', str(tr)):
            tr = ''
        else:
            return tr
    return tr
