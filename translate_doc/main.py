import docx
from google_trans_new import google_translator

import pandas as pd
import re
import translated

gog_translator = google_translator()

source_file = ['docs1.docx']
trans_file = 'dictionary.xlsx'

fulltext = []
uni_word = list()
# вводим переменную для индекса датафрейма
x = 0
# создаем датафрейм чтобы потом вставить туда исходные слова и перевод
df = pd.DataFrame(columns=['word', 'translate_word'])

for doc in source_file:
    # читаем файл
    gkz_doc = docx.Document(f'{doc}')
    # добавляем все параграфы в массив
    for paragraph in gkz_doc.paragraphs:
        fulltext.append(paragraph.text)

    # разбиваем тескт на отдельные слова
    st = str(fulltext).split(' ')
    # формируем уникальный список слов
    uni_word = list(set(st))

# перебираем массив слов и пишем их в датафрейм
for i in uni_word:
    # удаляем лишние знаки и переводим в нижний регистр
    a = i.replace(',', '').replace('.', '').replace('\'', '').replace(';', '').replace('[', '').replace(']', '').lower().replace(' ', '')
    # добавляем пару слов в датафрейм
    if re.fullmatch(r'([^0-9,\\/\-"():]*)', a) and re.fullmatch(r'([a-z]{4,})', a):
        tr = translated.translate(a)
        df.loc[x] = [a, tr]
        x = x + 1
    else:
        continue

# удаляем дубликаты слов по столбцу 'word'
df.drop_duplicates(subset='word')
# пишем получившуюся таблицу в файл эксель
df.to_excel(f'{trans_file}', merge_cells=False)

print(df)
